# Galician translations for debconf of kismet package.
# Copyright (C) 2012 THE kismet'S COPYRIGHT HOLDER
# This file is distributed under the same license as the kismet package.
#
# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: kismet\n"
"Report-Msgid-Bugs-To: kismet@packages.debian.org\n"
"POT-Creation-Date: 2019-07-26 09:16+0200\n"
"PO-Revision-Date: 2012-11-22 01:01+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#: ../kismet-capture-common.templates:2001
msgid "Install Kismet \"setuid root\"?"
msgstr "Quere instalar Kismet co bit «setuid root»?"

#. Type: boolean
#. Description
#: ../kismet-capture-common.templates:2001
msgid ""
"Kismet needs root privileges for some of its functions. However, running it "
"as root (\"sudo kismet\") is not recommended, since running all of the code "
"with elevated privileges increases the risk of bugs doing system-wide "
"damage. Instead Kismet can be installed with the \"setuid\" bit set, which "
"will allow it to grant these privileges automatically to the processes that "
"need them, excluding the user interface and packet decoding parts."
msgstr ""
"Kismet precisa privilexios de «root» para algunhas das súas funcións. Ainda "
"así, non se recomenda executalo como «root» («sudo kismet»), xa que executar "
"todo o código con privilexios elevados incrementa o risco de que un fallo "
"cause danos a todo o sistema. En lugar de iso, Kismet pode ser instalado co "
"bit «setuid», o que lle permitirá conceder eses privilexios automaticamente "
"aos procesos que o precisen, excluíndo a interface de usuario e as partes de "
"descodificación de paquetes."

#. Type: boolean
#. Description
#: ../kismet-capture-common.templates:2001
msgid ""
"Enabling this feature allows users in the \"kismet\" group to run Kismet "
"(and capture packets, change wireless card state, etc), so only thoroughly "
"trusted users should be granted membership of the group."
msgstr ""
"Activar esta funcionalidade permite aos usuarios no grupo «kismet» executar "
"Kismet (e capturar paquetes, cambiar o estado da tarxeta de rede sen fios, "
"etc...), polo que só se deberían engadir a este grupo usuarios de confianza."

#. Type: boolean
#. Description
#: ../kismet-capture-common.templates:2001
#, fuzzy
#| msgid ""
#| "For more detailed information, see section 4 of the Kismet README "
#| "(\"Suidroot & Security\"), which can be found at /usr/share/doc/kismet/"
#| "README or \"http://www.kismetwireless.net/README\"."
msgid ""
"For more detailed information, see the Kismet 010-suid.md, which can be "
"found at \"/usr/share/doc/kismet-doc/readme/010-suid.md\" in kismet-doc "
"package or \"https://www.kismetwireless.net/docs/readme/suid/\"."
msgstr ""
"Para obter información máis detallada consulte a sección 4 do ficheiro "
"README de Kismet («Suidroot & Security»), que pode atopar en «/usr/share/doc/"
"kismet/README» ou en «http://www.kismetwireless.net/README»."

#. Type: string
#. Description
#: ../kismet-capture-common.templates:3001
msgid "Users to add to the kismet group:"
msgstr "Usuarios a engadir ao grupo «kismet»:"

#. Type: string
#. Description
#: ../kismet-capture-common.templates:3001
msgid ""
"Only users in the kismet group are able to use kismet under the setuid model."
msgstr "No modelo «setuid» só os usuarios no grupo «kismet» poden usar Kismet."

#. Type: string
#. Description
#: ../kismet-capture-common.templates:3001
msgid ""
"Please specify the users to be added to the group, as a space-separated list."
msgstr ""
"Especifique mediante unha lista separada por espazos os usuarios que se "
"engadirán ao grupo."

#. Type: string
#. Description
#: ../kismet-capture-common.templates:3001
msgid ""
"Note that currently logged-in users who are added to a group will typically "
"need to log out and log in again before it is recognized."
msgstr ""
"Fíxese en que os usuarios que están actualmente rexistrados no sistema e son "
"engadidos a un grupo, normalmente terán que saír e volver a acceder para que "
"se lles recoñeza."

#. Type: error
#. Description
#: ../kismet-capture-common.templates:4001
msgid "The provided user list contains invalid usernames."
msgstr ""

#. Type: error
#. Description
#: ../kismet-capture-common.templates:4001
msgid ""
"The users to be added to the kismet group have to be provided in a space-"
"separated list of usernames. It seems that the following usernames are not "
"valid: ${USERS}. Please revise the list."
msgstr ""

#~ msgid ""
#~ "For more detailed information, see section 4 of the Kismet README "
#~ "(\"Suidroot & Security\"), which can be found at /usr/share/doc/kismet/"
#~ "README or \"http://www.kismetwireless.net/README\"."
#~ msgstr ""
#~ "Para obter información máis detallada consulte a sección 4 do ficheiro "
#~ "README de Kismet («Suidroot & Security»), que pode atopar en «/usr/share/"
#~ "doc/kismet/README» ou en «http://www.kismetwireless.net/README»."
